#include <stdio.h>

#define NUMCOUNTIES 23
#define NUMEVENTS 5

struct
  {
    char szCountyName[32];
    int nNotifiesSEOC;
    int nNotifiesEC;
    int nRepeaters;
    int nNotifyTrustee;
  }  rcCounties[NUMCOUNTIES] = {
  "Emmet",0,1,0,0,
  "Presque Isle",1,1,0,0,
  "Ottawa",0,0,0,0,
  "Macomb",1,1,0,0,
  "Muskegon",1,1,1,1,
  "Kent",1,1,0,0,
  "Bay",0,0,0,0,
  "Mackinac",1,1,0,0,
  "Wayne",1,1,1,0,
  "Alcona",0,1,1,1,
  "Benzie",0,1,0,0,
  "Allegan",0,0,0,0,
  "Oakland",1,1,0,0,
  "Monroe",0,1,0,0,
  "Tuscola",0,1,1,0,
  "Saginaw",1,1,1,1,
  "Arenac",0,0,0,0,
  "Crawford",0,0,0,0,
  "Clare",0,1,0,0,
  "Isabella",0,1,0,0,
  "Midland",0,1,1,0,
  "Washtenaw",0,1,0,0,
  "Kalamazoo",0,0,0,0
};

char szEvents[NUMEVENTS][64] = {
  "%s Sheriff and 911 looses cellular and radio communications",
  "%s notifies EC to stand by",
  "%s calls hotline to notify state of loss of comms",
  "EC %s observes loss of high profile repeaters",
  "EC %s calls repeater trustee to investigate"
};

int nElapsed[NUMCOUNTIES][NUMEVENTS];
int nCurMinutes;

void main()
{
  int i,j;
  int nTime, nHour, nMinutes;
  char szWork[80];

  nCurMinutes = 0;

  for ( i=0; i<NUMCOUNTIES; i++ )
    {
      nCurMinutes = i*17+3;
      nElapsed[i][0] = nCurMinutes;
      if ( rcCounties[i].nNotifiesEC )
	{
	  nCurMinutes +=11;
	  nElapsed[i][1] = nCurMinutes;
	}
      if ( rcCounties[i].nNotifiesSEOC )
	{
	  nCurMinutes +=12;
	  nElapsed[i][2] = nCurMinutes;
	}
      if ( rcCounties[i].nRepeaters )
	{
	  nCurMinutes += 12;
	  nElapsed[i][3] = nCurMinutes;
	}
      if ( rcCounties[i].nNotifyTrustee )
	{
	  nCurMinutes += 7;
	  nElapsed[i][3] = nCurMinutes;
	}
    }

  nCurMinutes = 320;
  for ( i=0; i<NUMCOUNTIES; i++ )
    for ( j=0; j<NUMEVENTS; j++ )
      {
	if ( nElapsed[i][j] > 0 )
	  {
	    nTime = nCurMinutes + nElapsed[i][j];
	    nHour = nTime / 60;
	    nMinutes = nTime % 60;
	    //printf("   %3d %3d %02d:%02d\n",i,j,nHour,nMinutes);
	    sprintf(szWork,szEvents[j],rcCounties[i].szCountyName);
	    if ( nHour<13 )
	      printf("%02d:%02d A.M. %s\n",nHour,nMinutes,szWork);
	    else
	      printf("%02d:%02d P.M. %s\n",nHour-12,nMinutes,szWork);
	    //printf("    <para>\n        <command>October 5, %d:%2d</command>\n    </para>\n",
	    //	   nHour,nMinutes);
	    //printf("    <para>\n        %s\n    </para>\n",szWork);
	  }
      }
}
