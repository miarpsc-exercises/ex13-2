#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define FILENAME "messagesraw.txt"

char words[26][15];
char szBuffer[256];
char szEnd[10][10] = { "X", "THIS", "IS", "A", "DRILL" };

void main()
{
  FILE *f;
  int i,wc,msgnum;

  f = fopen(FILENAME,"r");
  msgnum = 1000;
  while ( !feof(f) )
    {
      memset(words,0,sizeof(words));
      fgets(szBuffer,sizeof(szBuffer),f);
      if ( !feof(f) )
	{
	  sscanf(szBuffer,"%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s",
		 words[0],
		 words[1],
		 words[2],
		 words[3],
		 words[4],
		 words[5],
		 words[6],
		 words[7],
		 words[8],
		 words[9],
		 words[10],
		 words[11],
		 words[12],
		 words[13],
		 words[14],
		 words[15],
		 words[16],
		 words[17],
		 words[18],
		 words[19]);
	  wc = 0;
	  while ( strlen(words[wc]) > 0 )
	    {
	      //printf("\t%d [%s]\n",strlen(words[wc]),words[wc]);
	      wc++;
	    }
	  for ( i=0; i<5; i++ )
	    strcpy(words[wc+i],szEnd[i]);
	  wc += 5;
	  msgnum+=1;
	  printf("\n%d TP KD8MTL %d LANSING MI OCT 5\nSEOC\n=\n",msgnum,wc);
	  for ( i=0; i<wc; i++ )
	    {
	      printf("%s",words[i]);
	      if ( i%5 == 4 )
		printf("\n");
	      else
		printf(" ");
	    }
	  if ( wc%5 )
	    printf("\n");
	  printf("=\nEC\n");
	  printf("AR   N\n");
	  //printf("%d %s %s\n",wc,words[0],words[1]);
	}
    }
}
